﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using WmsVertereProjetoPadraoApi.Models.Usuario;

namespace WmsVertereProjetoPadraoApi.Common
{
    public class ValidaUser : IDisposable
    {
        public bool ValidarUser(string token)
        {

            return true;
            //validação do Token
        }

        public string GetUserToken(string token)
        {
            var jwtEncodedString = token.Substring(7);
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(jwtEncodedString);
            var tokenq = new JwtSecurityToken(jwtEncodedString: jwtEncodedString);
            var jti = tokenq.Claims.First(claim => claim.Type == "Usuario").Value;
            UsuarioModel user = JsonConvert.DeserializeObject<UsuarioModel>(jti);
            return user.ID.ToString();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                { }

                disposedValue = true;
            }
        }

        ~ValidaUser()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
