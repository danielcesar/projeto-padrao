﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace WmsVertereProjetoPadraoApi.Common
{
    public class AcessoDados
    {
        private IDbConnection ObterConexao()
        {
            try
            {
                IDbConnection conexao = null;
                {
                    switch (Base.TIPOBANCO)
                    {
                        case "SQL":
                            conexao = new SqlConnection(Base.STRINGCONEXAO);
                            break;
                        case "ORACLE":
                            conexao = new OracleConnection(Base.STRINGCONEXAO);
                            break;
                    }
                }
                conexao.Open();
                return conexao;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<T> Pesquisar<T>(string query)
        {
            try
            {
                IEnumerable<T> resultado;

                var conexao = ObterConexao();

                try
                {
                    resultado = conexao.Query<T>(query);
                }
                finally
                {
                    conexao.Close();
                    conexao.Dispose();
                }

                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<T> Pesquisar<T>(string consulta, object parametros = null, CommandType tipoComando = CommandType.StoredProcedure, bool comBuffer = true)
        {
            try
            {
                IEnumerable<T> resultado;

                var conexao = ObterConexao();

                try
                {
                    resultado = conexao.Query<T>(consulta, parametros, buffered: comBuffer, commandType: tipoComando);
                }
                finally
                {
                    conexao.Close();
                    conexao.Dispose();
                }

                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Executar(string query)
        {
            try
            {
                using (var conexao = ObterConexao())
                {
                    conexao.Query(query);
                    conexao.Close();
                }
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Executar(string procedimento, object parametros, CommandType tipoComando = CommandType.Text)
        {
            try
            {
                var conexao = ObterConexao();

                try
                {
                    conexao.Query(procedimento, parametros, commandType: tipoComando);
                }
                finally
                {
                    conexao.Close();
                    conexao.Dispose();
                }

                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Executar(Dictionary<string, object> query)
        {
            try
            {
                var conexao = ObterConexao();

                try
                {
                    foreach (KeyValuePair<string, object> t in query)
                    {
                        conexao.Execute(t.Key, t.Value);
                    }

                }
                finally
                {
                    conexao.Close();
                    conexao.Dispose();
                }

                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int GetMaxValue(string _field, string _table)
        {
            string query = "select nvl(max(" + _field + "),0)+1 AS MAX from " + _table + " WHERE ATIVO = 1";
            int i = 0;
            try
            {
                using (var conexao = ObterConexao())
                {
                    i = conexao.QueryFirst<int>(query, new { field = _field });
                    conexao.Close();
                }
                return i;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public string GetMaxValueString(string _field, string _table)
        {
            string query = "select max(" + _field + ") AS MAX from " + _table + " WHERE ATIVO = 1";
            string i = null;
            try
            {
                using (var conexao = ObterConexao())
                {
                    i = conexao.QueryFirst<string>(query, new { field = _field });
                    conexao.Close();
                }
                return i;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public string PesquisarFirstResult(Dictionary<string, object> query)
        {

            try
            {
                using (var conexao = ObterConexao())
                {
                    string i = null;
                    foreach (var item in query)
                    {
                        i = conexao.QueryFirst<string>(item.Key, item.Value);
                    }
                    conexao.Close();
                    return i;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<T> Pesquisar<T>(Dictionary<string, object> query)
        {
            try
            {
                var conexao = ObterConexao();
                IEnumerable<T> resultado = null;
                try
                {
                    foreach (KeyValuePair<string, object> t in query)
                    {
                        resultado = conexao.Query<T>(t.Key, t.Value);
                    }
                }
                finally
                {
                    conexao.Close();
                    conexao.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
