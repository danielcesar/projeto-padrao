﻿using System;
using System.Collections.Generic;
using WmsVertereProjetoPadraoApi.Models.TipoProduto;

namespace WmsVertereProjetoPadraoApi.Business.TipoProduto
{
    public class TipoProdutoBusiness : IDisposable
    {
        public string TipoProdutoValidaAdd(List<TipoProdutoModel> tipos)
        {
            foreach (TipoProdutoModel tipo in tipos)
            {
                if (String.IsNullOrEmpty(tipo.DESCRICAO) || tipo.DESCRICAO.Length > 30) return "DESCRICAO";
                if (String.IsNullOrEmpty(tipo.USUARIO)) return "USUARIO";
            }
            return "OK";
        }

        public string TipoProdutoValidaUpd(List<TipoProdutoModel> tipos)
        {
            foreach (TipoProdutoModel tipo in tipos)
            {
                if (tipo.ID == 0) return "ID";
                if (String.IsNullOrEmpty(tipo.DESCRICAO) || tipo.DESCRICAO.Length > 30) return "DESCRICAO";
                if (String.IsNullOrEmpty(tipo.USUARIO)) return "USUARIO";
            }
            return "OK";
        }

        public string TipoProdutoValidaDel(List<TipoProdutoModel> tipos)
        {
            foreach (TipoProdutoModel tipo in tipos)
            {
                if (tipo.ID == 0) return "ID";
                if (String.IsNullOrEmpty(tipo.USUARIO)) return "USUARIO";
            }
            return "OK";
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                { }

                disposedValue = true;
            }
        }

        ~TipoProdutoBusiness()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
