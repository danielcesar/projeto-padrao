﻿using System;
using System.Collections.Generic;
using WmsVertereProjetoPadraoApi.Business.TipoProduto;
using WmsVertereProjetoPadraoApi.Common;
using WmsVertereProjetoPadraoApi.DbScripts.TipoProduto;
using WmsVertereProjetoPadraoApi.Models.TipoProduto;

namespace WmsVertereProjetoPadraoApi.Repository.TipoProduto
{
    public class TipoProdutoRepository : AcessoDados, ITipoProdutoRepository, IDisposable
    {
        private TipoProdutoBusiness _tipoProdutoBusiness = new TipoProdutoBusiness();
        private TipoProdutoDbScript dbScript = new TipoProdutoDbScript();
        public TipoProdutoRepository() { Dispose(true); }

        public string TipoProdutoAdd(List<TipoProdutoModel> tipos)
        {            
            try
            {
                string retornoValidaAdd = _tipoProdutoBusiness.TipoProdutoValidaAdd(tipos);
                if (retornoValidaAdd != "OK")
                {
                    return retornoValidaAdd;
                }
                foreach (TipoProdutoModel tipo in tipos)
                {           
                    var retorno = Executar(dbScript.TipoProdutoAdd(tipo));
                }
                dbScript.Dispose();
                _tipoProdutoBusiness.Dispose();
                return "OK";

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string TipoProdutoUpd(List<TipoProdutoModel> tipos)
        {            
            try
            {
                string retornoValidaUpd = _tipoProdutoBusiness.TipoProdutoValidaUpd(tipos);
                if (retornoValidaUpd != "OK")
                {
                    return retornoValidaUpd;
                }
                foreach (TipoProdutoModel tipo in tipos)
                {
                    var retorno = Executar(dbScript.TipoProdutoUpd(tipo));
                }
                dbScript.Dispose();
                _tipoProdutoBusiness.Dispose();
                return "OK";

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string TipoProdutoDel(List<TipoProdutoModel> tipos)
        {            
            try
            {
                string retornoValidaDel = _tipoProdutoBusiness.TipoProdutoValidaDel(tipos);
                if (retornoValidaDel != "OK")
                {
                    return retornoValidaDel;
                }
                foreach (TipoProdutoModel tipo in tipos)
                {
                    var retorno = Executar(dbScript.TipoProdutoDel(tipo));
                }
                dbScript.Dispose();
                _tipoProdutoBusiness.Dispose();
                return "OK";

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<TipoProdutoModel> TipoProdutoGetTodos()
        {
            string SQL = "";
            try
            {
                SQL = dbScript.TipoProdutoGetTodos();
                var retorno = Pesquisar<TipoProdutoModel>(SQL);
                dbScript.Dispose();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IEnumerable<TipoProdutoModel> TipoProdutoGetById(int idTipoProduto)
        {
            try
            {
                Dictionary<string, object> dicionario = dbScript.TipoProdutoGetById(idTipoProduto);
                var retorno = Pesquisar<TipoProdutoModel>(dicionario);
                dbScript.Dispose();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                { }

                disposedValue = true;
            }
        }

        ~TipoProdutoRepository()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
