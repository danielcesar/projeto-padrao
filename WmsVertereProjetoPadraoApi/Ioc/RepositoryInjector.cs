﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WmsVertereProjetoPadraoApi.Repository.TipoProduto;

namespace WmsVertereProjetoPadraoApi.Ioc
{
    public class RepositoryInjector
    {
        public static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<ITipoProdutoRepository, TipoProdutoRepository>();
        }
    }
}
