﻿using WmsVertereProjetoPadraoApi.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;
using System;
using WmsVertereProjetoPadraoApi.Ioc;
using Microsoft.AspNetCore.Localization;
using System.Globalization;

namespace WmsVertereProjetoPadraoApi
{
    public class Startup
    {
        public IConfiguration _config { get; }
        public Startup(IConfiguration configuration)
        {
            _config = configuration;
            Base.TIPOBANCO = _config["Settings:TIPOBANCO"];
            switch (Base.TIPOBANCO)
            {
                case "SQL":
                    Base.STRINGCONEXAO = _config["ConnectionStrings:DPWCSQL"];
                    break;
                case "ORACLE":
                    Base.STRINGCONEXAO = _config["ConnectionStrings:DPWCOracle"];
                    break;
            }
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCors(options =>
            {
                options.AddPolicy("CorsAllowAll",
                builder => builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());
            });
            services.AddSwaggerGen(x => {
                x.SwaggerDoc("v1", new Info
                {
                    Title = "WMS Optimizer",
                    Version = "v1",
                    Description = "WMS - Projeto Optimizer - DP World Santos + Suzano",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "",
                        Email = "",
                        Url = ""
                    },
                    License = new License
                    {
                        Name = "DP World Santos + Suzano",
                        Url = ""
                    }

                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                x.IncludeXmlComments(xmlPath);
                x.OperationFilter<SwaggerFileOperationFilter>();
            });

            // Injecting repositories dependencies
            RepositoryInjector.RegisterRepositories(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("CorsAllowAll");
            app.UseSwagger();
            app.UseSwaggerUI(option => {
                option.SwaggerEndpoint("/swagger/v1/swagger.json", "WMS Optimizer - DP World Santos + Suzano");
                option.RoutePrefix = string.Empty;
            });

            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
